class Constants {
  static String apiBaseUrl = "https://bdew32324.webturtle.fr";
  static String uriProfil = "$apiBaseUrl/items/profil";

  static String uriFileTransfert = "$apiBaseUrl/files";
  static String uriAssets = "$apiBaseUrl/assets";
}
